BeautifulSoup4
psutil
python-potr
pyasn1
pycrypto
pyjavaproperties
pyparsing
pgpdump
qrcode >= 4.0.1
six
Pillow>=2.1.0
pymtp>=0.0.6